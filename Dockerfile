FROM openjdk:8-jdk-alpine


# cd /opt/app
WORKDIR /opt/app
COPY target/*.war app.war

# java -jar /opt/app/app.jar

ENTRYPOINT ["java","-jar","app.war"]
