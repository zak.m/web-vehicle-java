package com.example.web_app.Dao;

import com.example.web_app.pojo.User;

import org.springframework.data.jpa.repository.JpaRepository;


public interface UserDao extends JpaRepository<User, Integer>  {
    
}
