package com.example.web_app.Dao;

import com.example.web_app.pojo.MaterVehicle;

import org.springframework.data.repository.CrudRepository;
public interface MaterVehicleDao extends CrudRepository<MaterVehicle, String> {
    
}
// 