
package com.example.web_app.Dao;

import com.example.web_app.pojo.SessionDetail;

import org.springframework.data.repository.CrudRepository;

public interface SessionDetailRepo extends CrudRepository<SessionDetail, Long>  {
    
}
